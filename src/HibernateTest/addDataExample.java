package HibernateTest;

import java.sql.*;
import java.util.Calendar;

// I got this tutorial from http://alvinalexander.com/java/java-mysql-insert-example-preparedstatement/


public class addDataExample {




    public static class InsertExample
    {

        public static void main(String[] args)
        {
            try
            {
                // create a mysql database connection
                String myDriver = "org.gjt.mm.mysql.Driver";
                String myUrl = "jdbc:mysql://localhost/book";
                Class.forName(myDriver);
                Connection conn = DriverManager.getConnection(myUrl, "root", "root");


                // the mysql insert statement
                String query = " insert into book (id, name, author)"
                        + " values (?, ?, ?)";

                // create the mysql insert
                PreparedStatement preparedStmt = conn.prepareStatement(query);
                preparedStmt.setInt  (1, 8);
                preparedStmt.setString (2, "Harry");
                preparedStmt.setString   (3, "Potter");



                // execute the preparedstatement
                preparedStmt.execute();

                conn.close();
            }
            catch (Exception e)
            {
                System.err.println("Got an exception!");
                System.err.println(e.getMessage());
            }
        }
    }
}
