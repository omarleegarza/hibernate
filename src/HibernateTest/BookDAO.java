package HibernateTest;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import java.util.*;


public class BookDAO {

    SessionFactory factory = null;
    Session session = null;

    private static BookDAO single_instance = null;

    private BookDAO()
    {
        factory = HibernateUtils.getSessionFactory();
    }

    public static BookDAO getInstance()
    {
        if (single_instance == null) {
            single_instance = new BookDAO();
        }

        return single_instance;
    }

    /** Used to get more than one Book from database.*/
    public List<Book> getBooks() {

        try {
            session = factory.openSession();
            session.getTransaction().begin();
            String sql = "from HibernateTest.Book";
            List<Book> cs = (List<Book>)session.createQuery(sql).getResultList();
            session.getTransaction().commit();
            return cs;

        } catch (Exception e) {
            e.printStackTrace();
            // Rollback in case of an error occurred.
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }

    }

    /** Used to get a single Book from database */
    public Book getBook(int id) {

        try {
            session = factory.openSession();
            session.getTransaction().begin();
            String sql = "from HibernateTest.Book where id=" + Integer.toString(id);
            Book b = (Book)session.createQuery(sql).getSingleResult();
            session.getTransaction().commit();
            return b;

        } catch (Exception e) {
            e.printStackTrace();
            // Rollback in case of an error occurred.
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }
    }

}