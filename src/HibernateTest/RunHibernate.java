package HibernateTest;


import java.util.*;

public class RunHibernate {

    public static void main(String[] args) {

        BookDAO b = BookDAO.getInstance();

        List<Book> c = b.getBooks();
        for (Book i : c) {
            System.out.println(i);
        }

        System.out.println(b.getBook(7));
    }
}
